package pipeline

import (
	"bufio"
	"fmt"
	"io"
	"net/http"
	"strings"
	"sync"
)

func search(r io.Reader, word string) int {
	reader := bufio.NewReader(r)
	var count int
	for {
		s, err := reader.ReadString('\n')
		if err == io.EOF {
			return count
		}
		count += strings.Count(s, word)
	}
}

func SearchWord(urls <-chan string, word string) int {
	var count int
	result := make(chan int, len(urls))
	var wg sync.WaitGroup
	wg.Add(len(urls))
	for url := range urls {
		go func(result chan int, url string) {
			defer wg.Done()
			resp, err := http.Get(url)
			if err != nil {
				fmt.Printf("GET error: %v\n", err)
				return
			}
			result <- search(resp.Body, word)
			err = resp.Body.Close()
			if err != nil {
				fmt.Printf("error: %v\n", err)
				return
			}
		}(result, url)
	}
	wg.Wait()
	close(result)
	for r := range result {
		count += r
	}
	return count
}
